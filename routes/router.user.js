const express = require('express');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const randToken = require('rand-token');
const User = require('../model/model.user');
const Wallet = require('../utils/eth.generator');
const config = require('../config');
const router = express.Router();

function buildUser(req) {
    return new User({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        nickname: req.body.nickname,
        email: req.body.email,
        country: req.body.country,
        password: bcrypt.hashSync(req.body.password, 10),
        ip: req.body.ip,
        wallet: Wallet.generate_wallet(req.body.password)
    });
}

router.post('/register', function (req, res) {
    User.findOne({$or: [{email: req.body.email}, {nickname: req.body.nickname}]}).exec().then((doc, err) => {
        if (err || doc) return res.status(500).json({
            status: false
        });
        try {
            let user = buildUser(req);
            user.save().then(() => {
                return res.status(201).json({
                    status: true
                });
            }).catch(error => {
                return res.status(500).json({
                    error
                });
            });
        }
        catch (error) {
            return res.status(500).json({
                status: 'Error on fields'
            });
        }
    });

});

router.post('/login', function (req, res) {
    User.findOne({
        email: req.body.email
    }).then((user) => {
        if (bcrypt.compareSync(req.body.password, user.password)) {
            let token = jwt.sign({
                    email: user.email,
                    seed: user.wallet.seed,
                    _id: user._id
                },
                config.secret, {
                    expiresIn: '6h'
                });
            let refreshToken = randToken.uid(256);
            User.updateOne({email: user.email}, {$set: {refreshToken: refreshToken}}, {upsert: true}, function (err) {
                if (err) {
                    return res.status(500).json(err);
                } else {
                    return res.status(200).json({
                        "status": true,
                        "token": token,
                        "refreshToken": refreshToken
                    });
                }
            });
        } else {
            return res.status(500).json({
                status: false,
                message: 'Invalid data'
            });
        }
    }).catch(() => {
        return res.status(500).json({
            status: false
        });
    });
});


module.exports = router;