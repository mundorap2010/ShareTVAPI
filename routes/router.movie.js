const express = require('express');
const mongoose = require('mongoose');
const Movie = require('../model/model.movie');
const Report = require('../model/model.report');
const check = require('../utils/server.check');
const router = express.Router();

function shuffle(array) {
    return array.sort(function () {
        return 0.5 - Math.random()
    });
}

function buildReport(req) {
    return new Report({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
    });
}

function buildMovie(req) {
    return new Movie({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        openload: req.body.openload,
        rapidvideo: req.body.rapidvideo,
        streamango: req.body.streamango,
        key: check.check_url(req.body.key),
        img: req.body.img,
        puntuation: req.body.puntuation,
        year: req.body.year,
        director: req.body.director,
        synopsis: req.body.synopsis,
        trailer: req.body.trailer,
        size: req.body.size,
        quality: req.body.quality,
        language: req.body.language,
        uploader: req.body.uploader,
        genre: req.body.genre,
        classification: req.body.classification,
        producer: req.body.producer,
        writer: req.body.writer,
        cast: req.body.cast,
    });
}

router.get('/', function (req, res) {
    Movie.find({}, function (err, torrents) {
        res.send(shuffle(torrents));
    });
});

router.post('/search', function (req, res) {
    Movie.find({
        title: {
            '$regex': req.body.title,
            '$options': 'i'
        }
    }, function (err, doc) {
        if (err) return res.send(500, {
            error: err
        });
        return res.status(200).json({
            status: true,
            data: doc
        });
    });
});

router.post('/insert', function (req, res) {
    Movie.findOne({
        title: req.body.title
    }, function (err, doc) {
        if (doc) {
            return res.status(200).json({
                status: false,
                message: 'Movie already exist',
                movie: doc
            });
        } else {
            buildMovie(req).save().then(() => {
                return res.status(201).json({
                    status: true
                });
            }).catch(error => {
                return res.status(500).json({
                    error
                });
            });
        }
    });
});

router.post('/report', function (req, res) {
    buildReport(req).save().then(() => {
        return res.status(201).json({
            status: true
        });
    }).catch(error => {
        return res.status(500).json({
            error
        });
    });
});

router.get('/report', function (req, res) {
    Report.find({}, function (err, reports) {
        res.send(reports);
    });
});

router.put('/update', function (req, res) {
    req.body.key = check.check_url(req.body.key);
    Movie.findOneAndUpdate({
        title: req.body.title
    }, req.body, {
        upsert: false
    }, function (err) {
        if (err) return res.send(500, {
            error: err
        });
        return res.status(200).json({
            status: true
        });
    });
});

router.get('/genre/:genre', function (req, res) {
    Movie.find({
        genre: {
            $all: [req.params.genre]
        }
    }, function (err, torrents) {
        res.send(shuffle(torrents));
    });
});

module.exports = router;