const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const indexRouter = require('./routes/index');
const tvRouter = require('./routes/router.tv');
const movieRouter = require('./routes/router.movie');
const seriesRouter = require('./routes/router.serie');
const userRouter = require('./routes/router.user');

const app = express();
app.use(cors());
let server = require('http').createServer();
server.on('request', app);

mongoose.connect('mongodb://localhost:27017/sharetv', {useNewUrlParser: true}).then(() => {
    console.log('connected')
});

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/user', userRouter);
// app.use(require('./utils/token.check'));
app.use('/', indexRouter);
app.use('/tv', tvRouter);
app.use('/movie', movieRouter);
app.use('/series', seriesRouter);

server.listen(2828, '0.0.0.0', function () {
    const host = server.address().address;
    const port = server.address().port;
    console.log("ShareTV listening at http://%s:%s", host, port)
});
