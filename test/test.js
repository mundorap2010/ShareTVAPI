function return_url(url) {
    if (url.includes('rapidvideo')) {
        if (url.includes('https://www.rapidvideo.com/d/')) {
            return url.split('https://www.rapidvideo.com/d/')[1]
        } else if (url.includes('https://www.rapidvideo.com/embed/')) {
            return url.split('https://www.rapidvideo.com/embed/')[1]
        } else if (url.includes('https://www.rapidvideo.com/e/')) {
            return url.split('https://www.rapidvideo.com/e/')[1]
        } else if (url.includes('https://www.rapidvideo.com/v/')) {
            return url.split('https://www.rapidvideo.com/v/')[1]
        } else {
            return url
        }
    } else if (url.includes('streamango')) {
        if (url.includes('https://streamango.com/f/')) {
            return url.split('https://streamango.com/f/')[1]
        } else if (url.includes('https://streamango.com/embed/')) {
            return url.split('https://streamango.com/embed/')[1]
        } else {
            return url
        }
    }
}

function serie_link(seasons) {
    // faster and efficient loop array
    let i = 0;
    const iMax = seasons.length;
    for (; i < iMax; i++) {
        let j = 0;
        const jMax = seasons[i].caps.length;
        for (; j < jMax; j++) {
            let item = seasons[i].caps[j].key;
            seasons[i].caps[j].key = return_url(item);
        }
    }
    return seasons
}

let serie = {
    "director": [
        "Drew Goddard",
        "Steven S. DeKnight"
    ],
    "cast": [],
    "genre": [
        "Acción, Crimen"
    ],
    "_id": "5be2070f0477d56aabf86fe3",
    "title": "Daredevil",
    "img": "https://image.tmdb.org/t/p/w185/cidOqJL8tayqvv3TpfTQCsgeITu.jpg",
    "year": "2015",
    "synopsis": "Serie de TV (2015-Actualidad). Adaptación televisiva de los cómics del abogado de día, Matt Murdock, y superhéroe de noche, Daredevil.",
    "puntuation": "7.7",
    "trailer": "https://www.youtube.com/watch?v=un_smvHNuaE",
    "size": "1GB",
    "quality": "BRRIP",
    "uploader": "xperiafan13",
    "seasons": [
        {
            "caps": [
                {
                    "_id": "5be2070f0477d56aabf8700d",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONU6DT569",
                    "synopsis": "La vida de justiciero de Murdock y su nuevo despacho enfrentan desafíos igual de peligrosos en un caso de asesinato ligado al sindicato del crimen corporativo."
                },
                {
                    "_id": "5be2070f0477d56aabf8700c",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONU5VAQBW",
                    "synopsis": "Murdock comete un error casi fatal al intentar salvar a un chico secuestrado y encuentra un aliado improbable cuando tiene que escapar del peligro."
                },
                {
                    "_id": "5be2070f0477d56aabf8700b",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONU5ITA3I",
                    "synopsis": "Murdock y Foggy toman un cliente rico y misterioso, pero Murdock está convencido de que el caso no se limita solo a los hechos."
                },
                {
                    "_id": "5be2070f0477d56aabf8700a",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWIEBUU10V",
                    "synopsis": "Dos despiadados hermanos rusos que trabajan para Fisk contraatacan a Daredevil. Fisk avanza y consolida más su poder en el inframundo criminal."
                },
                {
                    "_id": "5be2070f0477d56aabf87009",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONU6M4TAM",
                    "synopsis": "Fisk progresa en su plan para destrozar Hell’s Kitchen. Murdock y Foggy aceptan un caso para defender a inquilinos victimizados por un casero abusivo."
                },
                {
                    "_id": "5be2070f0477d56aabf87008",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONU9P0NUD",
                    "synopsis": "Daredevil se ve enredado en el plan de Fisk para controlar Hell’s Kitchen. Ben Ulrich está cada vez más cerca de la verdad."
                },
                {
                    "_id": "5be2070f0477d56aabf87007",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONUA46FSB",
                    "synopsis": "Una persona importante en el pasado de Murdock reaparece y busca su ayuda para luchar contra un nuevo enemigo que amenaza Hell’s Kitchen."
                },
                {
                    "_id": "5be2070f0477d56aabf87006",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONUAWR72H",
                    "synopsis": "Mientras la misión de Murdock, Foggy y Karen se hace más clara, el mundo de Fisk se sale aún más de control en medio de la lucha de poder por Hell’s Kitchen."
                },
                {
                    "_id": "5be2070f0477d56aabf87005",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONUB5MNMJ",
                    "synopsis": "Cuando Fisk toma la delantera, las posibilidades de destruirlo se hacen cada vez más remotas. Mientras tanto, Daredevil se enfrenta a sus propios demonios."
                },
                {
                    "_id": "5be2070f0477d56aabf87004",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONUB4827E",
                    "synopsis": "La relación entre Murdock y Foggy se somete a la prueba más dificil cuando aparece un nuevo enemigo de Fisk."
                },
                {
                    "_id": "5be2070f0477d56aabf87003",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONUBDWIX2",
                    "synopsis": "Fisk y Murdock enfrentan las consecuencias de los caminos que eligieron, mientras Ben y Karen se acercan al verdadero pasado de Fisk."
                },
                {
                    "_id": "5be2070f0477d56aabf87002",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONUF3694N",
                    "synopsis": "Fisk busca venganza. Karen se siente atormentada por los acontecimientos recientes. Daredevil descubre algo perturbador en los manejos financieros de Fisk."
                },
                {
                    "_id": "5be2070f0477d56aabf87001",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONUEJKE3M",
                    "synopsis": "El final de temporada encuentra a Fisk acorralado, a Murdock desesperado y a Foggy y a Karen obligados también a jugar sus últimas cartas."
                }
            ],
            "_id": "5be2070f0477d56aabf87000"
        },
        {
            "caps": [
                {
                    "_id": "5be2070f0477d56aabf86fff",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONTXB4C6U",
                    "synopsis": "Una nueva amenaza ocupa el vacío que dejó Fisk en Hell’s Kitchen y aparece un cliente que, a pesar de su dudosa reputación, podría conducir hasta el fondo del asunto."
                },
                {
                    "_id": "5be2070f0477d56aabf86ffe",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONTX7TUI5",
                    "synopsis": "Mientras Murdock intenta recuperarse, Foggy y Karen protegen a un nuevo cliente que no solo está enfrentado con la ley, sino con el nuevo justiciero de Hell’s Kitchen."
                },
                {
                    "_id": "5be2070f0477d56aabf86ffd",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONTX1A172",
                    "synopsis": "Cara a cara con Punisher, Daredevil es arrastrado por el dilema de la justicia por mano propia, mientras Foggy y Karen arriesgan el cuello para salvar el estudio."
                },
                {
                    "_id": "5be2070f0477d56aabf86ffc",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWIECHCJSP",
                    "synopsis": "Daredevil piensa cuidadosamente sus próximos pasos mientras Karen sigue el débil rastro de Punisher, acechado por una sombra de venganza y muerte."
                },
                {
                    "_id": "5be2070f0477d56aabf86ffb",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONTXIW3P8",
                    "synopsis": "Un viejo amor de Murdock llega a Hell’s Kitchen y pone su mundo de cabeza. Karen, por su lado, sigue investigando a fondo a Punisher."
                },
                {
                    "_id": "5be2070f0477d56aabf86ffa",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONTXGT3MF",
                    "synopsis": "Al oponerse a la fiscalía, el futuro y el prestigio del estudio penden de un hilo, pero la justicia no se negocia. Karen habla a solas con Punisher."
                },
                {
                    "_id": "5be2070f0477d56aabf86ff9",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FT1UCZADZK",
                    "synopsis": "Mientras el estudio afronta el juicio del siglo con un acusado que no quiere cooperar, Murdock tiene cada vez más problemas para conciliar su doble identidad."
                },
                {
                    "_id": "5be2070f0477d56aabf86ff8",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FT1UCYR9EI",
                    "synopsis": "Cuando parece que nada podría ir peor, reaparece una importante influencia para Murdock con oscuros augurios sobre el futuro de Hell’s Kitchen."
                },
                {
                    "_id": "5be2070f0477d56aabf86ff7",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONU1YWS1O",
                    "synopsis": "Castle recibe una oferta imposible de rechazar. El futuro del estudio se escapa ante los ojos de Foggy y Murdock, pero Karen no se rinde tan fácilmente."
                },
                {
                    "_id": "5be2070f0477d56aabf86ff6",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONU1UQ2NF",
                    "synopsis": "Murdock y Foggy quedan atrapados en el fuego cruzado. Karen usa todos sus recursos para llegar a la verdad y Daredevil busca lo mismo, pero con otros métodos."
                },
                {
                    "_id": "5be2070f0477d56aabf86ff5",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FONU1X2ITV",
                    "synopsis": "La batalla de Punisher no da tregua y los cadáveres se apilan en las calles de Hell’s Kitchen. Murdock intenta terminar el trabajo que comenzó la fiscalía."
                },
                {
                    "_id": "5be2070f0477d56aabf86ff4",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FT1UCWD04G",
                    "synopsis": "La crisis del estudio es cada vez más grave. Karen sigue una pista que podría costarle caro. Daredevil se interna en las profundidades de la ciudad y de su propio pasado."
                },
                {
                    "_id": "5be2070f0477d56aabf86ff3",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FT1UD0GG5K",
                    "synopsis": "En el final de temporada, Daredevil encara un duelo decisivo del que no solo depende su propia vida, sino el futuro de Hell’s Kitchen."
                }
            ],
            "_id": "5be2070f0477d56aabf86ff2"
        },
        {
            "caps": [
                {
                    "_id": "5be2070f0477d56aabf86ff1",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWHBRL0ONW",
                    "synopsis": "Destrozado física y moralmente, Matt reflexiona sobre su propósito y su lugar en la Cocina del Infierno. Entretanto, Fisk pone en marcha un plan desde prisión."
                },
                {
                    "_id": "5be2070f0477d56aabf86ff0",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWHBRSMS77",
                    "synopsis": "Apesadumbrado por la vida que ha decidido abandonar, Matt sufre una crisis de fe. Tras hacer un trato con el FBI, Fisk se convierte en un objetivo."
                },
                {
                    "_id": "5be2070f0477d56aabf86fef",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWHBRQW65T",
                    "synopsis": "Cuando Fisk es trasladado a una residencia de lujo, Matt se debate contra los extremos hasta los que estaría dispuesto a llegar. Dex se centra en su objetivo."
                },
                {
                    "_id": "5be2070f0477d56aabf86fee",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWHBTMVMO0",
                    "synopsis": "Mientras Matt se infiltra en una cárcel para obtener información sobre los albaneses, Fisk pone a Dex en su punto de mira y Foggy pasa a la ofensiva."
                },
                {
                    "_id": "5be2070f0477d56aabf86fed",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWHBSE7VVA",
                    "synopsis": "Para apaciguar el creciente descontento con su puesta en libertad, Fisk brinda al FBI un cabeza de turco. Dex yerra el tiro cuando se topa con una mujer de su pasado."
                },
                {
                    "_id": "5be2070f0477d56aabf86fec",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWHBYSJ13S",
                    "synopsis": "Dex está al límite y totalmente perdido… hasta que Fisk le tiende la mano. Matt recurre a Karen, y esta acepta ayudarle, pero con una condición."
                },
                {
                    "_id": "5be2070f0477d56aabf86feb",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWHGN5UF8H",
                    "synopsis": "La prensa se ceba con Daredevil tras el ataque con el Bulletin. El agente Nadeem sospecha que el FBI pagó un precio demasiado alto por la cooperación de Fisk."
                },
                {
                    "_id": "5be2070f0477d56aabf86fea",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWHFZN10SN",
                    "synopsis": "Dex busca ayuda desesperadamente, Matt accede a una incómoda alianza con el agente Nadeem y Karen urde un peligroso plan para provocar a Fisk."
                },
                {
                    "_id": "5be2070f0477d56aabf86fe9",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWHBWFPRNN",
                    "synopsis": "El inestable mundo de Matt se derrumba tras enterarse de una increíble verdad, Karen huye para salvar la vida y Nadeem descubre hasta dónde llega la influencia de Fisk."
                },
                {
                    "_id": "5be2070f0477d56aabf86fe8",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWK02XOT78",
                    "synopsis": "Acosada por Fisk y los errores del pasado, Karen busca refugio en la iglesia. Matt consigue al fin una oportunidad y Dex se dispone a asestar el golpe de gracia."
                },
                {
                    "_id": "5be2070f0477d56aabf86fe7",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWHBWB0UVB",
                    "synopsis": "Dex trata de dar caza a su presa, Nadeem siente el peso de la conciencia y Fisk intenta recuperar un regalo de Vanessa que le requisaron durante su encierro."
                },
                {
                    "_id": "5be2070f0477d56aabf86fe6",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWHBX6O12S",
                    "synopsis": "El esperado reencuentro de Fisk no va según lo planeado. El bufete Nelson y Murdock acepta el caso de un nuevo cliente que posee información clave sobre Kingpin."
                },
                {
                    "_id": "5be2070f0477d56aabf86fe5",
                    "language": "Latino",
                    "openload": false,
                    "rapidvideo": true,
                    "streamango": false,
                    "key": "https://www.rapidvideo.com/d/FWHBYQNOSC",
                    "synopsis": "En el último episodio de la temporada, Matt se prepara para cruzar una línea sin retorno. Dex se vuelve más peligroso que nunca y Fisk ejecuta su jugada maestra."
                }
            ],
            "_id": "5be2070f0477d56aabf86fe4"
        }
    ],
    "created": "2018-11-06T21:26:39.079Z",
    "__v": 0
};

const a = serie_link(serie.seasons);
console.log(a[0]);