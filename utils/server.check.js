function return_url(url) {
    if (url.includes('rapidvideo')) {
        if (url.includes('https://www.rapidvideo.com/d/')) {
            return url.split('https://www.rapidvideo.com/d/')[1]
        } else if (url.includes('https://www.rapidvideo.com/embed/')) {
            return url.split('https://www.rapidvideo.com/embed/')[1]
        } else if (url.includes('https://www.rapidvideo.com/e/')) {
            return url.split('https://www.rapidvideo.com/e/')[1]
        } else if (url.includes('https://www.rapidvideo.com/v/')) {
            return url.split('https://www.rapidvideo.com/v/')[1]
        } else {
            return url
        }
    } else if (url.includes('streamango')) {
        if (url.includes('https://streamango.com/f/')) {
            return url.split('https://streamango.com/f/')[1]
        } else if (url.includes('https://streamango.com/embed/')) {
            return url.split('https://streamango.com/embed/')[1]
        } else {
            return url
        }
    } else if (url.includes('openload')) {
        if (url.includes('/openload.co/embed/')) {
            return url.split('https://openload.co/embed/')[1]
        } else if (url.includes('https://openload.co/f/')) {
            return url.split('https://openload.co/f/')[1]
        } else {
            return url
        }
    }
}


module.exports = {
    check_url: function (url) {
        return return_url(url)
    },
    series_link: function (seasons) {
        let i = 0;
        const iMax = seasons.length;
        for (; i < iMax; i++) {
            let j = 0;
            const jMax = seasons[i].caps.length;
            for (; j < jMax; j++) {
                let item = seasons[i].caps[j].key;
                seasons[i].caps[j].key = return_url(item);
            }
        }
        return seasons
    }
};