const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('../config');

module.exports = (req,res,next) => {
    const token = req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) {
                return res.status(401).json({
                    "error": true,
                    "message": 'Unauthorized access.'
                });
            }
            req.decoded = decoded;
            next();
        });
    } else {
        return res.status(403).send({
            err: true,
            message: 'No token provided.'
        });
    }
};
