const WavesAPI = require('@waves/waves-api');

module.exports = {
    generate_wallet: function (password) {
        const Waves = WavesAPI.create(WavesAPI.TESTNET_CONFIG);
        const seed = Waves.Seed.create();
        return {
            seed: seed.encrypt(password),
            address: seed.address,
            privateKey: seed.keyPair.privateKey,
            publicKey: seed.keyPair.publicKey
        }

    }
};