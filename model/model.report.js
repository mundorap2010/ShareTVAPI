const mongoose = require('mongoose');

const report = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {type: String, required: true},
});

module.exports = mongoose.model('Report', report);