const mongoose = require('mongoose');

const movie = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {type: String, required: true},
    director: [{type: String}],
    openload: {type: Boolean, required: true},
    rapidvideo: {type: Boolean, required: true},
    streamango: {type: Boolean, required: true},
    key: {type: String, required: true},
    puntuation: {type: String, required: true},
    img: {type: String, required: true},
    year: {type: String, required: true},
    synopsis: {type: String, required: true},
    trailer: {type: String, required: true},
    size: {type: String, required: true},
    quality: {type: String, required: true},
    language: {type: String, required: true},
    uploader: {type: String, required: true},
    created: { type: Date, default: Date.now },
    classification: {type: String, required: false},
    producer: {type: String, required: false},
    writer: [{type: String}],
    cast: [{type: String}],
    genre: [{type: String}],
});

module.exports = mongoose.model('Movie', movie);
