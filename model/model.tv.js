const mongoose = require('mongoose');

const tv = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {type: String, required: true},
    key: {type: String, required: true},
    img: {type: String, required: true}
});

module.exports = mongoose.model('TV', tv);